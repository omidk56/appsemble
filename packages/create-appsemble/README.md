# ![](https://gitlab.com/appsemble/appsemble/-/raw/0.18.23/config/assets/logo.svg) Appsemble Create

> Bootstrap an Appsemble block

[![GitLab CI](https://gitlab.com/appsemble/appsemble/badges/0.18.23/pipeline.svg)](https://gitlab.com/appsemble/appsemble/-/releases/0.18.23)
[![Code coverage](https://codecov.io/gl/appsemble/appsemble/branch/0.18.23/graph/badge.svg)](https://codecov.io/gl/appsemble/appsemble)
[![Prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://prettier.io)

## Usage

This package lets a developer bootstrap Appsemble related boilerplate using `npm init` or
`yarn create`.

```sh
npm init appsemble
```

```sh
yarn create appsemble
```

### Blocks

This package allows you to bootstrap an Appsemble block in a standard Appsemble project layout. The
initial command will be followed by a number questions interactively.

```sh
yarn create appsemble block
```

## License

[LGPL-3.0-only](https://gitlab.com/appsemble/appsemble/-/blob/0.18.23/LICENSE.md) ©
[Appsemble](https://appsemble.com)
