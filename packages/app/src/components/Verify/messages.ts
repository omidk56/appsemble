import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  requestFailed: 'Request failed. Token may be invalid.',
  requestSuccess: 'Your email was successfully verified!.',
  returnToApp: 'Return to app',
});
